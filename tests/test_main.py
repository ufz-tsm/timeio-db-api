#!/usr/bin/env python3
from __future__ import annotations
import json
import os
from typing import Any

import psycopg
from psycopg import sql
import pytest
import logging
from fastapi.testclient import TestClient
from psycopg.rows import dict_row

from app import main

logger = logging.getLogger(__name__)
logging.basicConfig(level="DEBUG")


@pytest.fixture(scope="session")
def db() -> psycopg.Connection:
    with psycopg.connect(os.environ["DB_URL"], connect_timeout=2) as conn:
        # clear_user_tables(conn)
        clear_user_tables(conn)
        yield conn


def clear_user_tables(conn: psycopg.Connection):
    conn.execute("truncate user_1.datastream, user_1.journal, user_1.observation")
    conn.commit()


@pytest.fixture(scope="session")
def client(db) -> TestClient:
    with TestClient(main.app) as c:
        yield c


@pytest.fixture(
    params=[
        "11111111-1111-1111-1111-111111111111",
        "22222222-2222-2222-2222-222222222222",
    ]
)
def thing_uuid(request):
    return request.param


def assert_valid_response(response):
    if response.status_code in [200, 201]:
        return
    pytest.fail(
        reason=f"response.statuscode: {response.status_code}\n"
        f"response.text: {response.text}"
    )


def get_observation_from_db(
    db, thing_uuid, result_time=None, user="user_1"
) -> list[dict[str, Any]] | dict[str, Any]:
    if result_time is None:
        cond = sql.SQL("true")
        args = [thing_uuid]
    else:
        cond = sql.SQL("result_time = %s")
        args = [thing_uuid, result_time]
    q = sql.SQL(
        """
        select o.*, d.position as datastream_pos
        from user_1.observation o
        join user_1.datastream d on o.datastream_id = d.id
        join user_1.thing t on d.thing_id = t.id
        where t.uuid = %s and {cond};
        """
    ).format(cond=cond)
    with db.cursor(row_factory=dict_row) as cur:
        if result_time:
            return cur.execute(q, args).fetchone()
        return cur.execute(q, args).fetchall()


def test_endpoint_health(client):
    response = client.get("/health")
    response.raise_for_status()
    assert_valid_response(response)
    assert response.json() == {"status": "ok"}


def test_endpoint_schema(client, thing_uuid):
    response = client.get(f"/schema/{thing_uuid}")
    assert_valid_response(response)
    assert response.json()["schema"] == "user_1"


@pytest.mark.parametrize(
    "ob",
    [
        {
            "result_time": "2020-10-10T00:00:00+00:00",
            "result_type": 0,
            "result_number": 42,
            "datastream_pos": "0",
        },
        {
            "result_time": "2020-10-10T00:00:01+00:00",
            "result_type": 0,
            "result_number": 42.42,
            "datastream_pos": "0",
        },
    ],
)
def test_endpoint_insert_number(client, db, thing_uuid, ob):
    data = {"observations": [ob]}
    response = client.post(f"/observations/insert/{thing_uuid}", json=data)
    assert_valid_response(response)
    from_db = get_observation_from_db(db, thing_uuid, ob["result_time"])
    assert from_db["result_time"].isoformat() == ob["result_time"]
    assert from_db["result_type"] == 0
    assert from_db["result_number"] == ob["result_number"]
    assert from_db["datastream_pos"] == "0"


@pytest.mark.parametrize(
    "ob",
    [
        {
            "result_time": "2020-10-10T00:11:00+00:00",
            "result_type": 1,
            "result_string": "test-string",
            "datastream_pos": "0",
        }
    ],
)
def test_endpoint_insert_string(client, db, thing_uuid, ob):
    data = {"observations": [ob]}
    response = client.post(f"/observations/insert/{thing_uuid}", json=data)
    assert_valid_response(response)
    from_db = get_observation_from_db(db, thing_uuid, ob["result_time"])
    assert from_db["result_time"].isoformat() == ob["result_time"]
    assert from_db["result_type"] == 1
    assert from_db["result_string"] == "test-string"
    assert from_db["datastream_pos"] == "0"


@pytest.mark.parametrize(
    "ob",
    [
        {
            "result_time": "2020-10-10T00:22:00+00:00",
            "result_type": 2,
            "result_json": json.dumps({"key": "value", "key1": None, "key3": {}}),
            "datastream_pos": "0",
        },
    ],
)
def test_endpoint_insert_json(client, db, thing_uuid, ob):
    data = {"observations": [ob]}
    response = client.post(f"/observations/insert/{thing_uuid}", json=data)
    assert_valid_response(response)
    from_db = get_observation_from_db(db, thing_uuid, ob["result_time"])
    assert from_db["result_time"].isoformat() == ob["result_time"]
    assert from_db["result_type"] == 2
    assert from_db["result_json"] == {"key": "value", "key1": None, "key3": {}}
    assert from_db["datastream_pos"] == "0"


@pytest.mark.parametrize(
    "ob",
    [
        {
            "result_time": "2020-10-10T00:33:00+00:00",
            "result_type": 3,
            "result_boolean": False,
            "datastream_pos": "0",
        }
    ],
)
def test_endpoint_insert_bool(client, db, thing_uuid, ob):
    data = {"observations": [ob]}
    response = client.post(f"/observations/insert/{thing_uuid}", json=data)
    assert_valid_response(response)
    from_db = get_observation_from_db(db, thing_uuid, ob["result_time"])
    assert from_db["result_time"].isoformat() == ob["result_time"]
    assert from_db["result_type"] == 3
    assert from_db["result_boolean"] == False  # noqa
    assert from_db["datastream_pos"] == "0"


@pytest.mark.parametrize(
    "obs",
    [
        [
            # duplicate observation
            {
                "result_time": "2020-10-10T00:00:55",
                "result_type": 0,
                "result_number": 42,
                "datastream_pos": "0",
            },
            {
                "result_time": "2020-10-10T00:00:55",
                "result_type": 0,
                "result_number": 42,
                "datastream_pos": "0",
            },
        ],
    ],
)
def test_endpoint_insert__duplicates_fail(client, thing_uuid, obs):
    data = {"observations": obs}
    response = client.post(f"/observations/insert/{thing_uuid}", json=data)
    assert response.status_code == 400


@pytest.mark.parametrize(
    # class Datastream(BaseModel):
    #     name: Optional[str] = None
    #     position: str
    #     properties: Optional[dict] = None
    "ds",
    [
        {
            "position": "foo",
        },
        {
            "name": "a datastream name",
            "position": "bar",
        },
        {
            "name": "another datastream name",
            "position": "99",
            "properties": {"key": None},
        },
    ],
)
def test_endpoint_create_datastream(client, db, thing_uuid, ds):
    response = client.post(f"/datastream/{thing_uuid}", json=ds)
    assert_valid_response(response)
    q = (
        "select d.* from user_1.datastream d "
        "join user_1.thing t on t.id = d.thing_id "
        "where t.uuid = %s and d.position = %s"
    )
    with db.cursor(row_factory=dict_row) as cur:
        from_db = cur.execute(q, [thing_uuid, ds["position"]]).fetchone()
    assert from_db != {}
    if name := ds.get("name"):
        assert from_db["name"] == name


@pytest.mark.parametrize(
    # class JournalEntry(BaseModel):
    #     timestamp: datetime
    #     level: str
    #     origin: str
    #     message: str
    "entry",
    [
        {
            "timestamp": "2020-10-10T11:11:00+00:00",
            "level": "warning",
            "origin": "from somewhere",
            "message": "the frooble fribbled",
        },
    ],
)
def test_endpoint_insert_journal(client, db, thing_uuid, entry):
    response = client.post(f"/journal/{thing_uuid}", json=entry)
    assert_valid_response(response)
    q = "select * from user_1.journal where timestamp = %s"
    with db.cursor(row_factory=dict_row) as cur:
        from_db = cur.execute(q, [entry["timestamp"]]).fetchone()
    assert from_db != {}
    for k in entry.keys():
        if k == "timestamp":
            assert from_db[k].isoformat() == entry[k]
        else:
            assert from_db[k] == entry[k]
