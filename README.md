# timeio-db-api

FastApi for the timeIO Observation Database


## Run locally with mocked DB

```shell
docker compose up -d --build 
```

## Run locally with external DB
Provide your DSN of a running DB in the environment variable `DB_URL`.


#### Docker compose 
Bear in mind, that you cannot use `localhost` nor `127.0.0.1` in the `DB_URL` now. 
This is, because the environment variable is passed into the docker image, but then
*localhost* will resolve to the system which exists **within** the image.
```shell
export DB_URL= ... 
docker compose up -d --build db-api
```

#### Plain python
```shell
export DB_URL= ... 
python app/main.py
```

## Run tests

```shell
docker compose up -d --build database-mock;
export DB_URL=postgresql://pg:pg@localhost:6661/pg;
pytest tests;
docker compose down --remove-orphans --timeout 0
```

### Healthcheck
[http://localhost:8002/health](http://localhost:8002/health)

### Docs

[http://localhost:8002/docs](http://localhost:8002/docs)

