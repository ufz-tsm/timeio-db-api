import decimal
from datetime import datetime
from pydantic import BaseModel, Json, model_validator
from typing import Optional, List, Any


class HealthCheckResponse(BaseModel):
    status: str


class Observation(BaseModel):
    phenomenon_time_start: Optional[datetime] = None
    phenomenon_time_end: Optional[datetime] = None
    result_time: datetime
    result_type: int
    result_number: Optional[decimal.Decimal] = None
    result_string: Optional[str] = None
    result_boolean: Optional[bool] = None
    result_json: Json[Any] = None
    result_latitude: Optional[decimal.Decimal] = None
    result_longitude: Optional[decimal.Decimal] = None
    result_altitude: Optional[decimal.Decimal] = None
    result_quality: Json[Any] = None
    valid_time_start: Optional[datetime] = None
    valid_time_end: Optional[datetime] = None
    parameters: Json[Any] = None
    datastream_pos: str

    @model_validator(mode="after")
    def check_result_column_match_result_type(self):
        n = self.result_number
        s = self.result_string
        b = self.result_boolean
        j = self.result_json
        if self.result_type == 0:
            if n is None or {s, b, j} != {None}:
                raise AssertionError(
                    f"If result_type is {self.result_type}, "
                    f"only result_number must not be empty."
                )
        elif self.result_type == 1:
            if s is None or {n, b, j} != {None}:
                raise AssertionError(
                    f"If result_type is {self.result_type}, "
                    f"only result_string must not be empty."
                )
        elif self.result_type == 2:
            if j is None or {s, b, n} != {None}:
                raise AssertionError(
                    f"If result_type is {self.result_type}, "
                    f"only result_json must not be empty."
                )
        elif self.result_type == 3:
            if b is None or {s, n, j} != {None}:
                raise AssertionError(
                    f"If result_type is {self.result_type}, "
                    f"only result_boolean must not be empty."
                )
        return self


class ObservationList(BaseModel):
    observations: List[Observation]


class QaqcLabel(BaseModel):
    result_time: datetime
    result_quality: Json[Any]
    datastream_id: int


class QaqcLabelList(BaseModel):
    qaqc_labels: List[QaqcLabel]


class Datastream(BaseModel):
    name: Optional[str] = None
    position: str
    properties: Optional[dict] = None


class JournalEntry(BaseModel):
    timestamp: datetime
    level: str
    origin: str
    message: str
