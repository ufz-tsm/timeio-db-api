get_schema_by_thing = """
    SELECT stm."schema" FROM public.schema_thing_mapping stm
    WHERE stm.thing_uuid = %(thing_uuid)s
"""

get_datastream_id_by_pos = """
    SELECT ds."id" FROM {schema}.datastream ds
    JOIN {schema}.thing t ON t."id"=ds.thing_id
    WHERE t."uuid"=%(thing_uuid)s 
    AND ds."position" = %(pos)s
"""

get_thing_id_and_name = """
    SELECT "id", "name" FROM {schema}.thing
    WHERE "uuid"=%(thing_uuid)s
"""


insert_datastream = """
    INSERT INTO {schema}.datastream AS d (
        "name", "position", "thing_id", "properties"
    ) VALUES (
        %(name)s, %(position)s, %(thing_id)s, %(properties)s::jsonb)
    RETURNING d."id"
"""

insert_observation = """
    INSERT INTO {schema}.observation AS o (
        phenomenon_time_start, phenomenon_time_end, result_time, result_type,
        result_number, result_string, result_boolean, result_json,
        result_latitude, result_longitude, result_altitude, result_quality,
        valid_time_start, valid_time_end, "parameters", datastream_id
    ) VALUES (
        %(phenomenon_time_start)s, %(phenomenon_time_end)s, %(result_time)s,
        %(result_type)s, %(result_number)s, %(result_string)s, %(result_boolean)s,
        %(result_json)s, %(result_latitude)s, %(result_longitude)s,
        %(result_altitude)s, %(result_quality)s, %(valid_time_start)s,
        %(valid_time_end)s, %(parameters)s, %(datastream_id)s
    )
    RETURNING o.datastream_id, o.result_time
"""

mass_insert_observations = """
    COPY {schema}.observation (
        phenomenon_time_start, phenomenon_time_end, result_time, result_type,
        result_number, result_string, result_boolean, result_json,
        result_latitude, result_longitude, result_altitude,
        result_quality, valid_time_start, valid_time_end,
        "parameters", datastream_id
    ) FROM STDIN
    """

create_tmp_upsert_table = """
    CREATE TEMP TABLE temp_obs (
    "phenomenon_time_start" timestamp with time zone NULL,
    "phenomenon_time_end"   timestamp with time zone NULL,
    "result_time"           timestamp with time zone NOT NULL,
    "result_type"           smallint                 NOT NULL,
    "result_number"         double precision         NULL,
    "result_string"         varchar(200)             NULL,
    "result_json"           jsonb                    NULL,
    "result_boolean"        boolean                  NULL,
    "result_latitude"       double precision         NULL,
    "result_longitude"      double precision         NULL,
    "result_altitude"       double precision         NULL,
    "result_quality"        jsonb                    NULL,
    "valid_time_start"      timestamp with time zone NULL,
    "valid_time_end"        timestamp with time zone NULL,
    "parameters"            jsonb                    NULL,
    "datastream_id"         bigint                   NOT NULL
    ) ON COMMIT DROP
    """


copy_tmp_upsert_table = """
    COPY temp_obs (
        phenomenon_time_start, phenomenon_time_end, result_time, result_type,
        result_number, result_string, result_boolean, result_json,
        result_latitude, result_longitude, result_altitude, result_quality,
        valid_time_start, valid_time_end, "parameters", datastream_id
    ) FROM STDIN
    """

mass_upsert_observations = """
    INSERT INTO {schema}.observation AS o (
        phenomenon_time_start, phenomenon_time_end, result_time, result_type,
        result_number, result_string, result_boolean, result_json,
        result_latitude, result_longitude, result_altitude, result_quality,
        valid_time_start, valid_time_end, "parameters", datastream_id
    ) SELECT DISTINCT ON (result_time, datastream_id) 
        phenomenon_time_start, phenomenon_time_end, result_time, result_type,
        result_number, result_string, result_boolean, result_json,
        result_latitude, result_longitude, result_altitude, result_quality,
        valid_time_start, valid_time_end, "parameters", datastream_id
      FROM temp_obs
      ON CONFLICT (result_time, datastream_id) DO UPDATE
      SET phenomenon_time_start = EXCLUDED.phenomenon_time_start,
        phenomenon_time_end = EXCLUDED.phenomenon_time_end,
        result_type = EXCLUDED.result_type,
        result_number = EXCLUDED.result_number,
        result_string = EXCLUDED.result_string,
        result_boolean = EXCLUDED.result_boolean,
        result_json = EXCLUDED.result_json,
        result_latitude = EXCLUDED.result_latitude,
        result_longitude = EXCLUDED.result_longitude,
        result_altitude = EXCLUDED.result_altitude,
        result_quality = EXCLUDED.result_quality,
        valid_time_start = EXCLUDED.valid_time_start,
        valid_time_end = EXCLUDED.valid_time_end,
        "parameters" = EXCLUDED.parameters
"""

create_tmp_qaqc_table = """
    CREATE TEMP TABLE temp_qaqc (
    "result_time"           timestamp with time zone NOT NULL,
    "result_quality"        jsonb                    NULL,
    "datastream_id"         bigint                   NOT NULL
    ) ON COMMIT DROP
    """

copy_tmp_qaqc_table = """
    COPY temp_qaqc (
        result_time, result_quality, datastream_id
    ) FROM STDIN
    """

add_qaqc_labels = """
    UPDATE {schema}.observation o
    SET result_quality = t.result_quality
    FROM temp_qaqc t
    WHERE o.datastream_id = t.datastream_id 
    AND o.result_time = t.result_time;
"""

insert_journal_entry = """
    INSERT INTO {schema}.journal AS j (
        "timestamp", "thing_id", "level", "origin", "message" 
    ) VALUES (
        %(timestamp)s, %(thing_id)s, %(level)s, %(origin)s, %(message)s)
    RETURNING j."id"
"""
