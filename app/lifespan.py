from contextlib import asynccontextmanager
from fastapi import FastAPI

import os
import logging

from psycopg_pool import AsyncConnectionPool
from psycopg.rows import dict_row

logger = logging.getLogger("db-api.database")


context = {"pool": None}


def get_connection_pool() -> AsyncConnectionPool:
    if (dsn := os.environ.get("DB_URL", None)) is None:
        raise EnvironmentError("Missing environment variable 'DB_URL'")
    return AsyncConnectionPool(
        dsn,
        kwargs={"row_factory": dict_row},
        open=False,
        check=AsyncConnectionPool.check_connection,
    )


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    Performs startup tasks for the app before processing
    incoming requests. Opens a connection pool to DB when
    app starts and closes it when the app stops.
    """
    logger.info("opening connection pool")
    async with get_connection_pool() as pool:
        context["pool"] = pool
        yield
    context["pool"] = None
