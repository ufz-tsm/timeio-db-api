#!/bin/bash

START_PERIOD=${HEALTH_START_PERIOD:-30}
INTERVAL=${HEALTH_INTERVAL:-30}
RETRIES=${HEALTH_RETRIES:-2}

cd /app || exit 2
uvicorn main:app --host 0.0.0.0 --port 8001 --reload &

sleep "$START_PERIOD"

retries=0
while true; do
  if ! wget -q http://0.0.0.0:8001/dbhealth -O /dev/null; then
    retries=$((retries+1))
    if [ $retries -gt "$RETRIES" ]; then
      echo "Failed to connect to database after $RETRIES retries. Terminating..."
      exit 1
    fi
    echo "Failed to connect to database. Retrying in $INTERVAL seconds..."
  else
    retries=0
  fi
    sleep "$INTERVAL"
done

