from __future__ import annotations
import logging
import json
import datetime
import asyncio

from psycopg import sql, AsyncConnection
from psycopg.errors import UniqueViolation
from psycopg.types.json import Jsonb
from typing import Any, Iterable
from lifespan import context

import queries
from models import Observation, QaqcLabel, JournalEntry

logger = logging.getLogger("db-api.database")


async def execute_and_fetchone(
    conn: AsyncConnection, query: str, schema: str, **kwargs
):
    kwargs = {
        k: None if v is None else Jsonb(v) if isinstance(v, dict) else v
        for k, v in kwargs.items()
    }
    q = sql.SQL(query).format(schema=sql.Identifier(schema))
    cur = await conn.execute(q, kwargs)
    return await cur.fetchone()


async def get_schema(conn: AsyncConnection, thing_uuid: str) -> str | None:
    cur = await conn.execute(queries.get_schema_by_thing, {"thing_uuid": thing_uuid})
    result = await cur.fetchone()
    if result is None:
        raise ValueError(f"no schema for thing {thing_uuid}")
    return result["schema"]


async def get_thing_info(
    conn: AsyncConnection, schema: str, thing_uuid: str
) -> dict | None:
    """Get name and id of a thing for a given thing_uuid."""
    q = queries.get_thing_id_and_name
    return await execute_and_fetchone(conn, q, schema, thing_uuid=thing_uuid)


async def get_datastream_id(
    conn: AsyncConnection, schema: str, thing_uuid: str, pos: str
) -> int | None:
    """Return the id of a datastream or None if it not exists."""
    q = queries.get_datastream_id_by_pos
    result = await execute_and_fetchone(conn, q, schema, thing_uuid=thing_uuid, pos=pos)
    if result:
        return result.get("id")
    return None


async def create_datastream(
    conn: AsyncConnection,
    schema: str,
    thing_uuid: str,
    pos: str,
    name: str | None = None,
) -> int:
    thing_info = await get_thing_info(conn, schema, thing_uuid)
    if thing_info is None:
        raise ValueError(f"Thing with UUID {thing_uuid} does not exist")
    properties = {"created_at": str(datetime.datetime.now())}

    if name is None:
        name = f"{thing_info['name']}/{pos}"

    # We use a fresh connection here because we want the
    # newly generated datastream be available immediately.
    # IOW we want commit the new datastream as soon as possible
    # (but we don't want to commit all changes we did before).
    # This also will reduce the chance of a race-condition, if
    # another service also want to create the same datastream.
    async with context["pool"].connection() as temp_conn:
        r = await execute_and_fetchone(
            temp_conn,
            queries.insert_datastream,
            schema,
            name=name,
            position=pos,
            thing_id=thing_info["id"],
            properties=None if properties is None else Jsonb(properties),
        )
    logger.info(f"Created new datastream (pos: {pos}) for {thing_uuid}.")
    return r.get("id")


async def get_or_create_datastream(
    conn: AsyncConnection, schema: str, thing_uuid: str, pos: str
) -> int:
    """Return datastream id. Creates datastream if it not exists."""
    ds_id = await get_datastream_id(conn, schema, thing_uuid, pos)
    if ds_id is None:
        try:
            ds_id = await create_datastream(conn, schema, thing_uuid, pos)
        except UniqueViolation:
            # If we come here, most likely we are in a race condition
            # with another service that just created this datastream.
            ds_id = await get_datastream_id(conn, schema, thing_uuid, pos)
    if ds_id is None:
        raise ValueError(f"could not create datastream (pos: {pos}) for {thing_uuid}")
    return ds_id


async def get_mapping_pos_to_id(
    conn: AsyncConnection, positions: list, schema: str, thing_uuid: str
):
    """
    Maps datastream_pos from request body to datastream_id
    from DB.
    """
    return {
        pos: await get_or_create_datastream(conn, schema, thing_uuid, pos)
        for pos in positions
    }


async def insert_observation(
    conn: AsyncConnection, observation: Observation, thing_uuid: str
) -> dict[str, str | int]:
    """
    Inserts an observation into the database.
    Missing datastreams will be created.
    """
    schema = await get_schema(conn, thing_uuid)
    ob_dict = observation.model_dump()
    pos = ob_dict.pop("datastream_pos")
    ds_id = await get_or_create_datastream(conn, schema, thing_uuid, pos)
    ob_dict["datastream_id"] = ds_id
    query = queries.insert_observation
    r = await execute_and_fetchone(conn, query, schema, **ob_dict)
    r["result_time"] = r["result_time"].isoformat(timespec="seconds")
    return r


async def insert_observations(
    conn: AsyncConnection,
    observations: Iterable[Observation],
    thing_uuid: str,
):
    schema = await get_schema(conn, thing_uuid)
    unique_pos = list(set(ob.datastream_pos for ob in observations))
    mapping = await get_mapping_pos_to_id(conn, unique_pos, schema, thing_uuid)
    query = sql.SQL(queries.mass_insert_observations).format(
        schema=sql.Identifier(schema)
    )
    async with conn.cursor() as cur:
        async with cur.copy(query) as copy:
            for ob in observations:
                await copy.write_row(
                    [
                        ob.phenomenon_time_start,
                        ob.phenomenon_time_end,
                        ob.result_time,
                        ob.result_type,
                        ob.result_number,
                        ob.result_string,
                        ob.result_boolean,
                        None if ob.result_json is None else Jsonb(ob.result_json),
                        ob.result_latitude,
                        ob.result_longitude,
                        ob.result_altitude,
                        None if ob.result_quality is None else Jsonb(ob.result_quality),
                        ob.valid_time_start,
                        ob.valid_time_end,
                        None if ob.parameters is None else Jsonb(ob.parameters),
                        mapping[ob.datastream_pos],
                    ]
                )


async def upsert_observations(
    conn: AsyncConnection,
    observations: Iterable[Observation],
    thing_uuid: str,
):
    schema = await get_schema(conn, thing_uuid)
    unique_pos = list(set(ob.datastream_pos for ob in observations))
    mapping = await get_mapping_pos_to_id(conn, unique_pos, schema, thing_uuid)
    query = sql.SQL(queries.mass_upsert_observations).format(
        schema=sql.Identifier(schema)
    )
    async with conn.cursor() as cur:
        await cur.execute(queries.create_tmp_upsert_table)
        async with cur.copy(queries.copy_tmp_upsert_table) as copy:
            for ob in observations:
                await copy.write_row(
                    [
                        ob.phenomenon_time_start,
                        ob.phenomenon_time_end,
                        ob.result_time,
                        ob.result_type,
                        ob.result_number,
                        ob.result_string,
                        ob.result_boolean,
                        None if ob.result_json is None else Jsonb(ob.result_json),
                        ob.result_latitude,
                        ob.result_longitude,
                        ob.result_altitude,
                        None if ob.result_quality is None else Jsonb(ob.result_quality),
                        ob.valid_time_start,
                        ob.valid_time_end,
                        None if ob.parameters is None else Jsonb(ob.parameters),
                        mapping[ob.datastream_pos],
                    ]
                )
        await cur.execute(query)


async def add_qaqc_labels(
    conn: AsyncConnection,
    labels: Iterable[QaqcLabel],
    thing_uuid: str,
):
    schema = await get_schema(conn, thing_uuid)
    query = sql.SQL(queries.add_qaqc_labels).format(schema=sql.Identifier(schema))
    async with conn.cursor() as cur:
        await cur.execute(queries.create_tmp_qaqc_table)
        async with cur.copy(queries.copy_tmp_qaqc_table) as copy:
            for l in labels:
                await copy.write_row(
                    [
                        l.result_time,
                        None if l.result_quality is None else Jsonb(l.result_quality),
                        l.datastream_id,
                    ]
                )
        await cur.execute(query)


async def insert_journal_entry(
    conn: AsyncConnection, entry: JournalEntry, thing_uuid: str
) -> int:
    schema = await get_schema(conn, thing_uuid)
    data_dict = entry.model_dump()
    thing_info = await get_thing_info(conn, schema, thing_uuid)
    if thing_info is None:
        raise ValueError(f"Thing with UUID {thing_uuid} does not exist")
    data_dict["thing_id"] = thing_info["id"]
    query = queries.insert_journal_entry
    if (jid := await execute_and_fetchone(conn, query, schema, **data_dict)) is None:
        raise ValueError(f"Database returned no journal.id")
    return jid
