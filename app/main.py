from typing import Annotated
from collections.abc import AsyncGenerator

from fastapi import FastAPI, status, Depends, Path, Body

import logging
import crud
import lifespan
from fastapi import HTTPException
from psycopg_pool import AsyncConnectionPool
from psycopg import AsyncConnection

from models import (
    HealthCheckResponse,
    Observation,
    ObservationList,
    QaqcLabelList,
    Datastream,
    JournalEntry,
)

logging.basicConfig(level="DEBUG")
logger = logging.getLogger("dbapi.main")

app = FastAPI(lifespan=lifespan.lifespan)


# dependencies
async def _get_connection(timeout: float = 2.0) -> AsyncGenerator[AsyncConnection]:
    pool: AsyncConnectionPool = lifespan.context["pool"]
    try:
        async with pool.connection(timeout=timeout) as c:
            yield c
    except TimeoutError as e:
        logger.exception(f"Failed to get connection within {timeout} seconds")
        raise HTTPException(status_code=500, detail="Failed to get connection") from e


# check if app is running
@app.get("/health", response_model=HealthCheckResponse)
async def health_check():
    return {"status": "ok"}


# check if db connection is working
@app.get("/dbhealth", response_model=HealthCheckResponse)
async def db_health_check(conn: Annotated[AsyncConnection, Depends(_get_connection)]):
    try:
        # Perform the SELECT 1 query to verify database connection
        async with conn.cursor() as c:
            await c.execute("SELECT 1")
            result = await c.fetchone()
        if result and next(iter(result.values())) == 1:
            return {"status": "ok"}
        else:
            raise HTTPException(status_code=500, detail=f"Unexpected database response")
    except Exception as e:
        logger.error(f"Database health check failed: {e}")
        raise HTTPException(status_code=500, detail="Database health check failed")


@app.get("/schema/{thing_uuid}", response_model=dict, status_code=status.HTTP_200_OK)
async def get_schema(
    thing_uuid: Annotated[str, Path(...)],
    conn: Annotated[AsyncConnection, Depends(_get_connection)],
):
    try:
        schema = await crud.get_schema(conn, thing_uuid)
    except Exception:
        msg = f"Thing with UUID {thing_uuid} does not exist."
        logger.exception(msg)
        raise HTTPException(status_code=404, detail=msg)

    logger.debug(f"Successfully got schema {schema}")
    return {"schema": schema}


@app.post("/observation/{thing_uuid}", status_code=status.HTTP_201_CREATED)
async def post_observation(
    observation: Annotated[Observation, Body(...)],
    thing_uuid: Annotated[str, Path(...)],
    conn: Annotated[AsyncConnection, Depends(_get_connection)],
):
    try:
        oid = await crud.insert_observation(conn, observation, thing_uuid)
    except Exception as e:
        msg = "Failed to insert observation"
        logger.exception(msg)
        raise HTTPException(status_code=400, detail=msg) from e

    logger.debug(f"successfully inserted observation {oid}")
    return oid


@app.post(
    "/observations/insert/{thing_uuid}",
    status_code=status.HTTP_201_CREATED,
    summary="Inserts observations for a given thing",
    description="Inserts new observations. Fails if any observation"
    "is already present in the database.",
)
async def insert_observations(
    observations: Annotated[ObservationList, Body(...)],
    thing_uuid: Annotated[str, Path(...)],
    conn: Annotated[AsyncConnection, Depends(_get_connection)],
):
    obs = observations.observations
    try:
        await crud.insert_observations(conn, obs, thing_uuid)
    except Exception as e:
        msg = "Failed to insert observations"
        logger.exception(msg)
        raise HTTPException(status_code=400, detail=msg) from e

    logger.debug(f"successfully inserted {len(obs)} observations ")
    return None


@app.post(
    "/observations/upsert/{thing_uuid}",
    status_code=status.HTTP_200_OK,
    summary="Upserts observations for a given thing",
    description="Inserts new observations or updates existing observations respectively.",
)
async def upsert_observations(
    observations: Annotated[ObservationList, Body(...)],
    thing_uuid: Annotated[str, Path(...)],
    conn: Annotated[AsyncConnection, Depends(_get_connection)],
):
    obs = observations.observations
    try:
        await crud.upsert_observations(conn, obs, thing_uuid)
    except Exception as e:
        msg = "Failed to upsert observations"
        logger.exception(msg)
        raise HTTPException(status_code=400, detail=msg) from e

    logger.debug(f"successfully upserted {len(obs)} observations ")
    return None


@app.post(
    "/observations/qaqc/{thing_uuid}",
    status_code=status.HTTP_200_OK,
    summary="Inserts QAQC labels for observations",
)
async def add_qaqc_labels(
    qaqc_labels: Annotated[QaqcLabelList, Body(...)],
    thing_uuid: Annotated[str, Path(...)],
    conn: Annotated[AsyncConnection, Depends(_get_connection)],
):
    labels = qaqc_labels.qaqc_labels
    try:
        await crud.add_qaqc_labels(conn, labels, thing_uuid)
    except Exception as e:
        msg = "Failed to add QAQC labels"
        logger.exception(msg)
        raise HTTPException(status_code=400, detail=msg) from e

    logger.debug(f"successfully added {len(labels)} QAQC labels to observations")
    return None


@app.post(
    "/datastream/{thing_uuid}",
    status_code=status.HTTP_201_CREATED,
    summary="Creates a new datastream for the given thing",
)
async def create_datastream(
    datastream: Annotated[Datastream, Body(...)],
    thing_uuid: Annotated[str, Path(...)],
    conn: Annotated[AsyncConnection, Depends(_get_connection)],
):
    try:
        schema = await crud.get_schema(conn, thing_uuid)
        datastream_id = await crud.create_datastream(
            conn, schema, thing_uuid, datastream.position, datastream.name
        )
    except Exception as e:
        msg = "Failed to insert datastream"
        logger.exception(msg)
        raise HTTPException(status_code=400, detail=msg) from e

    logger.debug(f"successfully inserted datastream {datastream_id}")
    return datastream_id


@app.post(
    "/journal/{thing_uuid}",
    status_code=status.HTTP_201_CREATED,
    summary="Add an entry in the projects journal",
)
async def insert_journal_entry(
    entry: Annotated[JournalEntry, Body(...)],
    thing_uuid: Annotated[str, Path(...)],
    conn: Annotated[AsyncConnection, Depends(_get_connection)],
):
    try:
        jid = await crud.insert_journal_entry(conn, entry, thing_uuid)
    except Exception as e:
        msg = "Failed to insert journal entry"
        logger.exception(msg)
        raise HTTPException(status_code=400, detail=msg) from e

    logger.debug(f"successfully inserted journal entry (id: {jid})")
    return jid


if __name__ == "__main__":
    # this is only executed if we run main directly. In contrast,
    # if we use uvicorn from shell, it just imports main, but do
    # not run it
    import uvicorn  # noqa

    uvicorn.run(app, host="0.0.0.0", port=8002)
