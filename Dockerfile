FROM python:3.11-alpine

# musl - free implementation of libc
# libpq - needed for postgres
RUN apk update && apk add bash gcc musl-dev libpq-dev

COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

RUN mkdir /app
COPY app /app

# make the app executable by any user
RUN adduser -D appuser
RUN chown -R appuser /app
USER appuser

ENV PGSSLROOTCERT=/etc/ssl/certs/ca-certificates.crt