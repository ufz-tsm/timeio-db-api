insert into public.schema_thing_mapping (schema, thing_uuid)
values ('user_1', '11111111-1111-1111-1111-111111111111'::uuid),
       ('user_1', '22222222-2222-2222-2222-222222222222'::uuid)
;

insert into user_1.thing (name, uuid, description, properties)
values ('Thing1', '11111111-1111-1111-1111-111111111111'::uuid, 'T1',
        '{}'::jsonb),
       ('Thing2', '22222222-2222-2222-2222-222222222222'::uuid, 'T2', '{}'::jsonb)
;