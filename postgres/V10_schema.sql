begin;

create table if not exists public.schema_thing_mapping
(
    "schema"     varchar(100) not null,
    "thing_uuid" uuid         not null,
    unique ("schema", "thing_uuid")
);

drop schema if exists user_1;
create schema user_1;
set search_path to user_1;

create table "thing"
(
    "id"          bigserial    not null primary key,
    "name"        varchar(200) not null,
    "uuid"        uuid         not null unique,
    "description" text         null,
    "properties"  jsonb        null
);
create table "datastream"
(
    "id"          bigserial    not null primary key,
    "name"        varchar(200) not null,
    "description" text         null,
    "properties"  jsonb        null,
    "position"    varchar(200) not null,
    "thing_id"    bigint       not null
);
alter table "datastream"
    add constraint "datastream_thing_id_position_9f2cfe68_uniq" unique ("thing_id", "position");
alter table "datastream"
    add constraint "datastream_thing_id_f55522a4_fk_thing_id" foreign key ("thing_id") references "thing" ("id") deferrable initially deferred;
create index "datastream_thing_id_f55522a4" on "datastream" ("thing_id");

create table "observation"
(
    "id"                    bigserial                not null primary key,
    "phenomenon_time_start" timestamp with time zone null,
    "phenomenon_time_end"   timestamp with time zone null,
    "result_time"           timestamp with time zone not null,
    "result_type"           smallint                 not null,
    "result_number"         double precision         null,
    "result_string"         varchar(200)             null,
    "result_json"           jsonb                    null,
    "result_boolean"        boolean                  null,
    "result_latitude"       double precision         null,
    "result_longitude"      double precision         null,
    "result_altitude"       double precision         null,
    "result_quality"        jsonb                    null,
    "valid_time_start"      timestamp with time zone null,
    "valid_time_end"        timestamp with time zone null,
    "parameters"            jsonb                    null,
    "datastream_id"         bigint                   not null
);
alter table "observation"
    add constraint "observation_datastream_id_result_time_1d043396_uniq" unique ("datastream_id", "result_time");
alter table "observation"
    add constraint "observation_datastream_id_77f5c4fb_fk_datastream_id" foreign key ("datastream_id") references "datastream" ("id") deferrable initially deferred;
create index "observation_datastream_id_77f5c4fb" on "observation" ("datastream_id");

create table "journal"
(
    "id"        bigserial                not null primary key,
    "timestamp" timestamp with time zone not null,
    "level"     varchar(30)              not null,
    "message"   text                     null,
    "extra"     jsonb                    null,
    "thing_id"  bigint                   not null,
    "origin"    varchar(200)             null
);
alter table "journal"
    add constraint "journal_thing_id_fk_thing_id" foreign key ("thing_id") references "thing" ("id") deferrable initially deferred;
create index "journal_thing_id" on "journal" ("thing_id");

commit;

